<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Contact Book</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style>
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}

/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {
	height: 450px
}

/* Set gray background color and 100% height */
.sidenav {
	padding-top: 20px;
	background-color: #f1f1f1;
	height: 100%;
}

/* Set black background color, white text and some padding */
footer {
	background-color: #555;
	color: white;
	padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
	.sidenav {
		height: auto;
		padding: 15px;
	}
	.row.content {
		height: auto;
	}
}

label.error {
	float: none;
	color: red;
}
</style>
<script src="https://code.jquery.com/jquery-3.1.0.js"
	integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>

<script type="text/javascript">
	$(function() {
		$("#idBtnSearch").click(function(){
			$.ajax({
				url:"SearchContact",
				mrthod:"get",
				data:{
					searchStr:$("#idSearchStr").val()
				},
				success:function(data){
					$("#idSearchResult").html(data);
				}
			})
		})
		$("#idBtnDelete").click(function(){
			//alert("ok");
			$.ajax({
				url:"ContactDelete",
				mrthod:"get",
				data:{
					idDelete:$(this).attr(id)
				}
			})
		})
		$("#idAddContact").validate({
			rules:{
				fullname:{
					required:true,
					minlength:3,
					maxlength:20
				},
				email:{
					required:true,
				},
				mobile:{
					required:true,
					minlength:10,
					maxlength:15
				}
			},
			messages:{
				fullname:{
					required:"Name can not be empty",
					minlength:"Name should have Min 3 characters",
					maxlength:"Name can not be more than 20 characters"
				},
				email:{
					required:"Email id can not be empty",
				},
				mobile:{
					required:"Mobile can not be empty"
				}
			}
		})
	})
</script>
</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Logo</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidenav">
				<p>
					<a href="addContact" class="btn btn-primary">View Contact</a>
				</p>

			</div>
			<div class="col-sm-8 text-left">
				<div>
					<h2 class="well">Contact Information</h2>
				</div>
				<button class="btn btn-primary pull-right">Add Contact</button><br>
				<div class="form-group">
					<label for="contain">Search Contacts</label> <input
						class="form-control" type="text" id="idSearchStr" />
				</div>
				<input type="button" id="idBtnSearch" value="Search"/>
					<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
				
				<div id="idSearchResult"></div>
				
				<table class="table">
					<thead>
						<tr>
							<th>Cuid</th>
							<th>FULLNAME</th>
							<th>EMAIL</th>
							<th>MOBILE</th>
							<th>EDIT</th>
							<th>DELETE</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${allcontact}" var="contc">
							<tr>
								<td><p id="idDelete">${contc.getCuid()}</p></td>
								<td>${contc.getFullName()}</td>
								<td>${contc.getEmail()}</td>
								<td>${contc.getMobile()}</td>
								<td><span class="glyphicon glyphicon-edit"></span></td>
								<td><!-- <span class="glyphicon glyphicon-remove"> --><input type="button" id="idBtnDelete" value="Delete" /></span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<br>
				<div class="col-sm-2"></div>
				<div class="col-sm-9">
					<form action="addContact" method="post" id="idAddContact">
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 form-control-label">Full
								Name :</label>
							<div class="col-sm-10">
								<input type="text" name="fullname" class="form-control"
									id="fullname" placeholder="Enter Full Nmae Ex:John" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 form-control-label">Email
								:</label>
							<div class="col-sm-10">
								<input type="email" name="email" class="form-control" id="email"
									placeholder="Enter Email Ex:xyz@example.com" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 form-control-label">Mobile
								:</label>
							<div class="col-sm-10">
								<input type="number" name="mobile" class="form-control"
									id="mobile" placeholder="Enter Mobile Number Ex:099999XXXXX"
									required>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">Save</button>
								<button type="reset" class="btn btn-primary">Reset</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
			<div class="col-sm-2 sidenav"></div>
		</div>
	</div>

	<footer class="container-fluid text-center">
		<p>Spaneos Software Solutions Pvt. Ltd. &copy; 2015 Spaneos All
			Rights Reserved</p>
	</footer>

</body>