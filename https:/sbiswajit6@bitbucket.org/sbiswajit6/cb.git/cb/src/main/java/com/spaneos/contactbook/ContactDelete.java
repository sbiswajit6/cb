package com.spaneos.contactbook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ContactDelete
 */
@WebServlet("/ContactDelete")
public class ContactDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<Contact> contactlist=new ArrayList<>();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ContactDao c= new ContactDaoImpl();
		//String idDelete= request.getParameter("idDelete");
		int cid=Integer.parseInt(request.getParameter("idDelete"));
		System.out.println(cid);
		c.deleteContact(cid);
		contactlist=c.getAll();
		request.setAttribute("allcontact", contactlist);
		RequestDispatcher rd= request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
