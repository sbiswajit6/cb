package com.spaneos.contactbook;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ContactDaoImpl implements ContactDao {

	private ConnectionUtil dbUtil=ConnectionUtil.conUtilObj;
	private static final Logger LOGGER=LoggerFactory.getLogger(ContactDaoImpl.class);
	private static final String ADD_CONTACT="insert into CONTACT(FULLNAME,EMAIL,MOBILE) values(?,?,?)";
	private static final String DELETE_CONTACT="delete from CONTACT where CUID=?";
	private static final String SEARCHALL_CONTACT="select * from CONTACT";
	private static final String GET_CONTACT="select * from CONTACT where CUID=?";
	private static final String UPDATE_CONTACT="UPDATE CONTACT SET FULLNAME=? ,EMAIL=?, MOBILE=? WHERE CUID=?";
	private static final String SEARCH_CONTACTS = "select * from CONTACT where FULLNAME=? or "+ "EMAIL=?";
	@Override
	public boolean addContact(Contact contact) {
		
		Connection con=null;
		PreparedStatement pst=null;
		int res=0;
		try {
			con=dbUtil.getConnection();
			pst=con.prepareStatement(ADD_CONTACT);
			pst.setString(1, contact.getFullName());
			pst.setString(2, contact.getEmail());
			pst.setString(3, contact.getMobile());
			res=pst.executeUpdate();
		} catch (SQLException e) {
			
			LOGGER.info("While adding {}",e);
		}
		finally {
			dbUtil.close(pst, con);
		}
		return res!=0;
	}
	@Override
	public boolean deleteContact(int cuid) {
		
		Connection con=null;
		PreparedStatement pst=null;
		int res=0;
		try {
			con=dbUtil.getConnection();
			pst=con.prepareStatement(DELETE_CONTACT);
			pst.setInt(1, cuid);
			res=pst.executeUpdate();
		} catch (SQLException e) {
			
			LOGGER.info("While Delete {}",e);
		}
		finally {
			dbUtil.close(pst, con);
		}
		return res!=0;
	}
	@Override
	public List<Contact> search(String searchStr) {
		Connection con = null;
		PreparedStatement st= null;
		ResultSet res=null;
		List<Contact> list = new ArrayList<>();
		try {
			con = dbUtil.getConnection();
			st= con.prepareStatement(SEARCH_CONTACTS);
			st.setString(1, searchStr);
			st.setString(2, searchStr);
			res= st.executeQuery();
			while(res.next()){
				Contact contact = new Contact();
				contact.setCuid(res.getInt("CUID"));
				contact.setFullName(res.getString("FULLNAME"));
				contact.setEmail(res.getString("EMAIL"));
				contact.setMobile(res.getString("MOBILE"));
				list.add(contact);
			}
		} catch (SQLException e) {
			LOGGER.info("While serching : {}",e);
		}finally{
			dbUtil.close(st, con);
		}
		return list;
		
		
	}
	@Override
	public Contact updateContact(Contact contact) {
		Connection con = null;
		PreparedStatement pst= null;
		int res=0;

		try {
			con = dbUtil.getConnection();
			pst= con.prepareStatement(UPDATE_CONTACT);
			pst.setString(1,contact.getFullName());
			pst.setString(2, contact.getEmail());
			pst.setString(3, contact.getMobile());
			pst.setInt(4, contact.getCuid());
			res= pst.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.info("While updating: {}",e);
			e.printStackTrace();
		}finally{
			dbUtil.close(pst, con);
		}
		
		return res==0?null:contact;
	}
	@Override
	public List<Contact> getAll() {
		Connection con = null;
		Statement st= null;
		ResultSet res=null;
		List<Contact> list = new ArrayList<>();
		try {
			con = dbUtil.getConnection();
			st= con.createStatement();
			res= st.executeQuery(SEARCHALL_CONTACT);
			while(res.next()){
				Contact contact = new Contact();
				contact.setCuid(res.getInt("CUID"));
				contact.setFullName(res.getString("FULLNAME"));
				contact.setEmail(res.getString("EMAIL"));
				contact.setMobile(res.getString("MOBILE"));
				list.add(contact);
			}
		} catch (SQLException e) {
			LOGGER.info("While serching all the contact: {}",e);
		}finally{
			dbUtil.close(st, con);
		}
		return list;
	}
	@Override
	public Contact get(int cuid) {
		Connection con = null;
		PreparedStatement st= null;
		ResultSet res=null;
		Contact contact = new Contact();
		try {
			con = dbUtil.getConnection();
			st= con.prepareStatement(GET_CONTACT);
			st.setInt(1, cuid);
			res= st.executeQuery();
			
			while(res.next()){
				contact.setCuid(res.getInt("CUID"));
				contact.setFullName(res.getString("FULLNAME"));
				contact.setEmail(res.getString("EMAIL"));
				contact.setMobile(res.getString("MOBILE"));
			}
		} catch (SQLException e) {
			LOGGER.info("While searching : {}",e);
			e.printStackTrace();
		}finally{
			dbUtil.close(st, con);
		}
		return contact;
	
	}

	
}
