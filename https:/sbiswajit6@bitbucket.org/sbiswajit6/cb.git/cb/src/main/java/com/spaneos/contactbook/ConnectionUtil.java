package com.spaneos.contactbook;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;

import java.sql.CallableStatement;
import java.sql.Connection;

public enum ConnectionUtil {
	conUtilObj;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionUtil.class);
	private ConnectionUtil(){
		
	}
	static{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			LOGGER.error("While loading driver : {}",e.getMessage());
		}
	}
	
	public Connection getConnection(){
		Connection con=null;
		try {
			con = DriverManager.getConnection("jdbc:mysql://192.168.1.224:3306/spanjdbc","root","spaneos");
		} catch (SQLException e) {
			LOGGER.info("While connecting with database : {}",e.getMessage());
		}
		return con;
	}
	public void close(Statement st,Connection con){
		try {
			if(st!=null){
				st.close();
			}
			if(con!=null){
				con.close();
			}
		} catch (SQLException e) {
			
			LOGGER.info("While Closing the database : {}",e.getMessage());
		}
	}
	public void close(ResultSet rs,Statement st,Connection con){
		try {
			if(st!=null){
				st.close();
			}
			if(con!=null){
				con.close();
			}
			if(rs!=null){
				rs.close();
			}
		} catch (SQLException e) {
			
			LOGGER.info("While Closing the database : {}",e.getMessage());
		}
	}
	public void close(PreparedStatement pst,Connection con){
		try {
			if(pst!=null){
				pst.close();
			}
			if(con!=null){
				con.close();
			}
		} catch (SQLException e) {
			
			LOGGER.info("While Closing the database : {}",e.getMessage());
		}
	}
	public void close(ResultSet rs,PreparedStatement pst,Connection con){
		try {
			if(pst!=null){
				pst.close();
			}
			if(con!=null){
				con.close();
			}
			if(rs!=null){
				rs.close();
			}
		} catch (SQLException e) {
			
			LOGGER.info("While Closing the database : {}",e.getMessage());
		}
	}
	public void close(CallableStatement cst,Connection con){
		try {
			if(cst!=null){
				cst.close();
			}
			if(con!=null){
				con.close();
			}
		} catch (SQLException e) {
			
			LOGGER.info("While Closing the database : {}",e.getMessage());
		}
	}
	public void close(ResultSet rs,CallableStatement cst,Connection con){
		try {
			if(cst!=null){
				cst.close();
			}
			if(con!=null){
				con.close();
			}
			if(rs!=null){
				rs.close();
			}
		} catch (SQLException e) {
			
			LOGGER.info("While Closing the database : {}",e.getMessage());
		}
	}
}
