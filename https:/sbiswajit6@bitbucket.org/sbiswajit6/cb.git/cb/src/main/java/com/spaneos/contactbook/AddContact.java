package com.spaneos.contactbook;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddContact
 */
@WebServlet(urlPatterns = {"", "/addContact"})
public class AddContact extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	List<Contact> contactlist=new ArrayList<>();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//if(contactlist.isEmpty()){
		//	out.println("<h3>No record found</h3>");
		//}
		//else{
		
			ContactDao c=new ContactDaoImpl();
			contactlist=c.getAll();
			request.setAttribute("allcontact", contactlist);
			RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
		//}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String fname=request.getParameter("fullname");
		String email=request.getParameter("email");
		String mobile=request.getParameter("mobile");
		Contact contact=new Contact();
		contact.setFullName(fname);
		contact.setEmail(email);
		contact.setMobile(mobile);
		//contactlist.add(contact);
		ContactDao c=new ContactDaoImpl();
		boolean result=c.addContact(contact);
		request.setAttribute("res", result);
		RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	}

}
